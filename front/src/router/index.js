import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/sign',
      name: 'sign',
      component: HomeView
    },
    {
      path: '/',
      name: 'main',

      component: () => import('../views/MainPage.vue')
    },
    {
      path: '/newscreate',
      name: 'NewsCreate',

      component: () => import('../views/PostNews.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/news',
      name: 'News',

      component: () => import('../views/NewsGetAll.vue')
    },
    {
      path: '/news/:news_id',
      name: 'NewsDetail',

      component: () => import('../modules/News/NewsDetail.vue')
    },
    {
      path: '/news/:news_id/update',
      name: 'NewsUpdate',
      component: () => import('../modules/News/NewsUpdate.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/users',
      name: 'Users',

      component: () => import('../views/UsersList.vue'),
      meta: { requiresAuth: true, allowedRoles: [1] }
    },
    {
      path: '/users/:user_id',
      name: 'UserDetail',

      component: () => import('../modules/user/UserDetail.vue'),
      meta: { requiresAuth: true, allowedRoles: [1] }
    },
    {
      path: '/users/:user_id/update',
      name: 'UserUpdate',
      component: () => import('../modules/user/UserUpdate.vue'),
      meta: { requiresAuth: true, allowedRoles: [1] }
    },
    {
      path: '/createusers',
      name: 'СreateUsers',

      component: () => import('../modules/user/PostUsers.vue'),
      meta: { requiresAuth: true, allowedRoles: [1] }
    },
    {
      path: '/createtasks',
      name: 'createtasks',

      component: () => import('../modules/task/CreateTasks.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },

    {
      path: '/tasks',
      name: 'tasks',

      component: () => import('../views/TaskList.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/tasks/:task_id',
      name: 'TaskDetails',
      component: () => import('../modules/task/TaskDetails.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/tasks/:task_id',
      name: 'TaskUpdate',
      component: () => import('../modules/task/TaskUpdate.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/company',
      name: 'company',

      component: () => import('../views/company/AboutCompany.vue')
    },

    {
      path: '/history',
      name: 'history',

      component: () => import('../views/company/HistoryInfo.vue')
    },
    {
      path: '/management',
      name: 'management',

      component: () => import('../views/company/ManagementInfo.vue')
    },
    {
      path: '/management/:management_id',
      name: 'detailmanagement',

      component: () => import('../modules/management/DetailManagement.vue')
    },
    {
      path: '/management/:management_id/update',
      name: 'managementUpdate',
      component: () => import('../modules/management/UpdateManagement.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/createmanagement',
      name: 'createmanagement',

      component: () => import('../modules/management/PostManagement.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/mission',
      name: 'mission',

      component: () => import('../views/company/MissionAndStrategy.vue')
    },
    {
      path: '/partners',
      name: 'partners',

      component: () => import('../views/partners/TermsOfCooperation.vue')
    },
    {
      path: '/partnernew',
      name: 'partnernew',

      component: () => import('../views/partners/HowToBecomeAPartner.vue')
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'notfound',

      component: () => import('../modules/System/NotFound.vue')
    },
    {
      path: '/403',
      name: '403',

      component: () => import('../modules/System/NotRights.vue')
    },
    {
      path: '/blogs',
      name: 'blogs',

      component: () => import('../views/BlogList.vue')
    },
    {
      path: '/blogcreate',
      name: 'blogcreate',

      component: () => import('../modules/blog/BlogPost.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/blogs/:post_id',
      name: 'blogDetail',

      component: () => import('../modules/blog/BlogGetId.vue')
    },
    {
      path: '/blogs/:post_id/update',
      name: 'blogUpdate',
      component: () => import('../modules/blog/BlogUpdate.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/job',
      name: 'job',

      component: () => import('../views/company/JobOpenings.vue')
    },
    {
      path: '/jobcreate',
      name: 'jobcreate',

      component: () => import('../modules/vacancy/CreateVacancy.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/job/:vacancy_id',
      name: 'jobDetail',

      component: () => import('../modules/vacancy/VacancyDetail.vue')
    },
    {
      path: '/job/:vacancy_id/update',
      name: 'jobUpdate',
      component: () => import('../modules/vacancy/UpdateVacancy.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/applications',
      name: 'applications',

      component: () => import('../views/ApplicationsList.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/applicationscreate',
      name: 'applicationscreate',

      component: () => import('../modules/applications/ApplicationsCreate.vue')
    },
    {
      path: '/applications/:application_id',
      name: 'applicationsDetail',

      component: () => import('../modules/applications/ApplicationsDetail.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },

    {
      path: '/review',
      name: 'review',

      component: () => import('../views/company/ReviewsList.vue')
    },
    {
      path: '/reviewcreate',
      name: 'reviewcreate',

      component: () => import('../modules/reviews/CreateReview.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/review/:review_id',
      name: 'reviewDetail',

      component: () => import('../modules/reviews/DetailReview.vue')
    },
    {
      path: '/review/:review_id/update',
      name: 'reviewUpdate',
      component: () => import('../modules/reviews/UpdateReview.vue')
    },
    {
      path: '/promotions',
      name: 'promotions',

      component: () => import('../views/PromotionsList.vue')
    },
    {
      path: '/promotionscreate',
      name: 'promotionscreate',

      component: () => import('../modules/promotions/PromotionsCreate.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/promotions/:promotion_id',
      name: 'promotionsDetail',

      component: () => import('../modules/promotions/PromotionsDetail.vue')
    },
    {
      path: '/promotions/:promotion_id/update',
      name: 'promotionsUpdate',
      component: () => import('../modules/promotions/UpdatePromotions.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/products',
      name: 'products',

      component: () => import('../views/CatalogList.vue')
    },
    {
      path: '/productcreate',
      name: 'productcreate',

      component: () => import('../modules/catalog/CatalogPost.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/products/:product_id',
      name: 'productsDetail',

      component: () => import('../modules/catalog/CatalogDetail.vue')
    },
    {
      path: '/products/:product_id/update',
      name: 'productsUpdate',
      component: () => import('../modules/catalog/CatalogUpdate.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/personal',
      name: 'personal',
      component: () => import('../views/PersonalArea.vue'),
      meta: { requiresAuth: true }
    },

    {
      path: '/services',
      name: 'services',

      component: () => import('../views/company/ServicesList.vue')
    },
    {
      path: '/servicescreate',
      name: 'servicescreate',

      component: () => import('../modules/services/ServicesPost.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/services/:services_id',
      name: 'servicesDetail',

      component: () => import('../modules/services/ServicesDetail.vue')
    },
    {
      path: '/services/:services_id/update',
      name: 'servicesUpdate',
      component: () => import('../modules/services/ServicesUpdate.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/positions',
      name: 'positions',

      component: () => import('../modules/positions/PositionAll.vue'),
      meta: { requiresAuth: true, allowedRoles: [1] }
    },
    {
      path: '/positions/:id/update',
      name: 'positionsUpdate',
      component: () => import('../modules/positions/PositionUpdate.vue'),
      meta: { requiresAuth: true, allowedRoles: [1] }
    },
    {
      path: '/furniture',
      name: 'furniture',

      component: () => import('../modules/furniture/FurnitureALL.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    },
    {
      path: '/furniture/:type_id/update',
      name: 'furnitureUpdate',
      component: () => import('../modules/furniture/FurnitureUpdate.vue'),
      meta: { requiresAuth: true, allowedRoles: [1, 2] }
    }
  ]
})

const checkAuthentication = () => {
  const isAuthenticated = localStorage.getItem('token') !== null
  const roleIdString = localStorage.getItem('role_id')
  const role_id = roleIdString ? parseInt(roleIdString) : null
  return { isAuthenticated, role_id }
}

router.beforeEach((to, from, next) => {
  const { isAuthenticated, role_id } = checkAuthentication()

  if (to.meta.requiresAuth) {
    if (!isAuthenticated) {
      next('/403')
    } else if (to.meta.allowedRoles && !to.meta.allowedRoles.includes(role_id)) {
      next('/403')
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
