import axios from 'axios'
import router from './router'

const api = axios.create({
  baseURL: 'http://localhost:3000'
})

// Добавляем перехватчик запросов
api.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('token')
    if (token) {
      // Если токен есть, добавляем его к заголовкам запроса
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  (error) => {
    return Promise.reject(error) // Возвращаем ошибку для дальнейшей обработки
  }
)

// Добавляем перехватчик ответов
api.interceptors.response.use(
  (response) => response, // Возвращает успешный ответ без изменений
  (error) => {
    if (error.response) {
      const status = error.response.status
      if (status === 401) {
        // Если получен статус 401 (Unauthorized), перенаправляем пользователя на главную страницу
        router.push('/')
      } else if (status === 403) {
        router.push('/403')
        // Если получен статус 403 (Forbidden), обрабатываем ошибку здесь
        // Например, можно вывести сообщение об ошибке или выполнить какие-то другие действия
        console.error('Error 403: Forbidden')
      }
    }
    return Promise.reject(error) // Возвращаем ошибку для дальнейшей обработки
  }
)

export default api
